/*
  CPT211 Programming Language Concepts & Paradigms
  Assignment 2
  Galileo Web Sever Morse Code Control
 
  created by
  Team Name    : LeoTrio
  Team Leader  : Koay Bee Bee   - 115035
  Team Members : Wong Kean Yi   - 115142
                 Chong Tiam Fei - 115009
*/

#include <SPI.h>
#include <Ethernet.h>

int led = 13; // LED connected to digital pin 13
String buffer = "";
String current = "";

// MAC address and IP address for the controller
// The IP address will be dependent on the local network
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress ip(10,207,134,61);

// Initialize the Ethernet server library
// with the IP address and port we want to use 
// (port 80 is default for HTTP)
EthernetServer server(80);

void setup() {
  // Open serial communications and wait for port to open
  Serial.begin(9600);
  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();
  pinMode(led,OUTPUT); // sets the digital pin as output
}

void loop() {
  // listen for incoming clients
  EthernetClient client = server.available();
  digitalWrite(led, HIGH); // sets the LED on
 
  if (client) {
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    current = "";
    
    while (client.connected()) {
     
      if (client.available()) {
        char c = client.read();
        buffer+=c; 
        Serial.write(c);

        if (c == '\n' && currentLineIsBlank){
          //send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type:text/html");
          client.println("Connection:close");
          client.println();
          // add a meta refresh tag, so the browser pulls again every 5 seconds:
          client.println("<meta http-equiv=\"refresh\" content=\"3; URL='http://10.207.134.61' \" />");
          client.println("<font size=\"5\"> <b>Galileo Web Sever Morse Code Control</b> </font>");
          client.println("<br />"); // insert a line break
          client.println("<i>by LeoTrio</i>");
          client.println("<br />");
          client.println("<br />");
          client.println("<fieldset>");
          client.println("<legend> <font size=\"4\"> <b> Message: </b> </font> </legend>");
          client.println("Please choose a message and click the Submit button. <br /> <br />");
          client.print("<FORM action=\"http://10.207.134.61\" >");
          client.print("<P> <INPUT type=\"radio\" name=\"status\" value=\"0\" >Hello World");
          client.print("<P> <INPUT type=\"radio\" name=\"status\" value=\"1\" >SOS");
          client.print("<P> <INPUT type=\"radio\" name=\"status\" value=\"2\" >I am fabulous");
          client.print("<P> <INPUT type=\"submit\" value=\"Submit\" onclick=\"this.value='Running...'; this.disabled='disabled'; this.form.submit(); \" />");
          client.print("</FORM>");
          client.print("</fieldset>");
          client.println("<P> Status:");
          client.print(current);
          break;
        }//end if
        
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
          buffer = ""; // clear the buffer at end of line
        }
        else if (c == '\r') {
        
          if(buffer.indexOf("GET /?status=0")>=0) {
            current="'Hello World' signal has completed";
            Serial.println();
            Serial.println("Hello World: ");
            helloWorld();
          }  
          if(buffer.indexOf("GET /?status=1")>=0) {
            current="'SOS' signal has completed";
            Serial.println();
            Serial.println("SOS: ");
            SOS();
          }
          if(buffer.indexOf("GET /?status=2")>=0) {
            current="'I am Fabulous' has completed";
            Serial.println();
            Serial.println("I am Fabulous: ");
            iAmFabulous();
          } 
        }
        else{
          // you've gotten a character on the current line
          currentLineIsBlank = false; 
        }
      }//end if
    }//end while

    //give the web browser time to receive the data
    delay(1);
    //close the connection:
    client.stop();
  }//end if
}//end loop

// short signal, dots/dits
void dit(){
    Serial.print("."); // print "." for dits in the serial monitor
    digitalWrite(led,HIGH);
    delay(200);
    digitalWrite(led,LOW);
    delay(200);
}

// long signal, dashes/dahs
void dah(){
    Serial.print("-"); // print "-" for dahs in the serial monitor
    digitalWrite(led,HIGH);
    delay(600);
    digitalWrite(led,LOW);
    delay(200);
}

void letterDelay(){ 
  // pause between letters is 3 units of time (3 * 200 = 600)
  delay(400); // 600 - 200 (delay in dit/dah) = 400
}

void wordDelay(){
  // pause between words is 7 units of time (7 * 200 = 1400)
  delay(1200); // 1400 - 200 (delay in dit/dah) = 1200
}
 
void helloWorld() {
  dit();
  dit();
  dit();
  dit();
  letterDelay();
  dit();
  letterDelay();
  dit();
  dah();
  dit();
  dit();
  letterDelay();
  dit();
  dah();
  dit();
  dit();
  letterDelay();
  dah();
  dah();
  dah();
  wordDelay();
  dit();
  dah();
  dah();
  letterDelay();
  dah();
  dah();
  dah();
  letterDelay();
  dit();
  dah();
  dit();
  letterDelay();
  dit();
  dah();
  dit();
  dit();
  letterDelay();
  dah();
  dit();
  dit();
  wordDelay();
} 
    
void SOS() {
  dit();
  dit();
  dit();
  letterDelay();
  dah();
  dah();
  dah();
  letterDelay();
  dit();
  dit();
  dit();
  wordDelay();
}

void iAmFabulous() {
  dit();
  dit();
  wordDelay();
  dit();
  dah();
  letterDelay();
  dah();
  dah();
  wordDelay();
  dit();
  dit();
  dah();
  dit();
  letterDelay();
  dit();
  dah();
  letterDelay();
  dah();
  dit();
  dit();
  dit();
  letterDelay();
  dit();
  dit();
  dah();
  letterDelay();
  dit();
  dah();
  dit();
  dit();
  letterDelay();
  dah();
  dah();
  dah();
  letterDelay();
  dit();
  dit();
  dah();
  letterDelay();
  dit();
  dit();
  dit();
  wordDelay();
}
